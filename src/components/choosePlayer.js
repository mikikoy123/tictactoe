import React, {Component} from 'react';

class Player extends Component{
    handleForm(e){
        e.preventDefault();
        this.props.player(e.target.player.value)
    }
    render(){
        return (
            <form onSubmit={(e) => this.handleForm(e)}>
                <label>Player X (1)
                <input type="radio" name="player" value="X"></input>
                </label>
                <label>Player O (2)
                <input type="radio" name="player" value="O"></input>
                </label>
                <input type="submit" value="Start"></input>
            </form>
        );
    }
}

export default Player;